# The SOLID Principles of Object-Oriented Programming

 The Solid Principles are consists of five set of rules. Which is used to design the Object-Oriented class design structure.
 These principles help software design to make more understandable, flexible and maintainable.
 
 ### Five principles are:
 
 - Single-responsibility principle
 - Open–closed principle
 - Liskov substitution principle
 - Interface segregation principle
 - Dependency inversion principle

###  Single-responsibility principle
The Single-responsibility principle states that every module, class or function should do only one thing. It should have one single reason to change.

This means that if a class is a data container, like a Book class or a Student class, and it has some fields regarding that entity, it should change only when we change the data model.

Following this principle is important because in a team many members may work on the same class and try to change it for a different purpose and it may lead to the incompatible module.

We can understand with an example suppose there is a module which compiles and creates a report, so the module could change for two reasons. One is if the content of the report is changed and Second reason is if the format of the report is changed.

 ~~~java
 class Book {
	String name;
	int price;
	public Book(String name,int price) {
		this.name = name;
        this.price = price;
	}
}

 ~~~
 Here we can see that a simple  Book class is created and inside there is information about the books.
 ~~~java
public class Invoice {

	private Book book;
	private int quantity;
	private double gst;
	private double total;

	public Invoice(Book book, int quantity,double gst) {
		this.book = book;
		this.quantity = quantity;
		this.gst = gst;
		this.total = this.calculateTotal();
	}
	public double calculateTotal() {
	        double price = quantity * price;
		double priceWithTaxes = price + gst;
		return priceWithTaxes;
	}

	public void printInvoice() {
            System.out.println(quantity + "x " + book.name + " " +book.price);
            System.out.println("Ta: " + gst);
            System.out.println("Total: " + total);
	}
        public void saveToFile(String filename) {
	// Creates a file with given name and writes the invoice
	}
}
 ~~~
Here we can clearly see that it violates the Single Responsibility Principle. According to the principle, our class should have one reason to change and here the reason should be invoice but here the calculation of price logic is also present.

We can resolve this issue by separating different works into a different class.
~~~java
public class InvoicePrinter {
    private Invoice invoice;
    public InvoicePrinter(Invoice invoice) {
        this.invoice = invoice;
    }
    public void print() {
        System.out.println(invoice.quantity + "x " + invoice.book.name + " " + invoice.book.price);
        System.out.println("Tax: " + invoice.gst);
        System.out.println("Total: " + invoice.total);
    }
}
~~~
Now it follows SRP principle, every class is responsible for one aspect.



### Open–closed principle
The Open-Closed Principle requires that classes should be open for extension and closed to modification.
Modification means that changing the code and extensions means that adding new features.
With help of interfaces and abstract classes, we can achieve that.

Let us see the previous example and understand if we want to add new functionality like we want to store the invoices into a database.
~~~java
public class InvoiceSavingDatabase {
    Invoice invoice;
    public InvoiceSavingDatabase(Invoice invoice) {
        this.invoice = invoice;
    }
    public void saveToFile(String filename) {
        // Creates a file with given name and writes the invoice
    }
    public void saveToDatabase() {
        // Saves the invoice to database
    }
}
~~~
In this, we can modify the class to create an interface to follow the principle.

~~~java
interface InvoiceSavingDatabase {
    public void save(Invoice invoice);
}
~~~

Here we have created an interface and now every method which implements InvoiceSavingDatabase can provide implementation to the save().
~~~java
public class DatabaseStore implements InvoiceSavingDatabase {
    public void save(Invoice invoice) {
        // Save to DB
    }
}
~~~
~~~java
public class FilePersistence implements InvoicePersistence {
    public void save(Invoice invoice) {
        // Save to file
    }
}
~~~
Now our InvoiceSavingDatabase logic can easily be extendable, and we can add this to more than one database using interfaces.
This provides flexibility in terms of polymorphism.

### Liskov Substitution Principle
The Liskov Substitution Principle states that subclasses should be substitutable for their base classes.
If **S** is a subtype of **T**, then objects of type **T** may be replaced with objects of type **S** (i.e., an object of type T may be substituted with any object of a subtype S).

Let us understand this principle with an example:
~~~java
    class Vehicles
    {
       String name;
       String getName() { ... }
       void setName(String n) { ... }
       double speed;
       double getSpeed() { ... }
       void setSpeed(double d) { ... }
       
       Engine engine;
       Engine getEngine() { ... }
       void setEngine(Engine e) { ... }
       void startEngine() { ... }
    }
    
    class Car extends Vehicles
    {
       void startEngine() { ... }
    }
~~~
here we can see that a car is also a vehicle and it can extend Vehicle class and override the startEngine().
~~~java
    class Bicycle extends TransportationDevice
    {
       void startEngine();
    }
~~~
We can not use startEngine() here as Bicycle is not a vehicle.

We can solve this problem by providing a proper inheritance hierarchy.
~~~java
    class TrasportationVehicles
    {
       String name;
       String getName() { ... }
       void setName(String n) { ... }
       double speed;
       double getSpeed() { ... }
       void setSpeed(double d) { ... }
    }
~~~
~~~java
    class VehiclesWithoutEngines extends TrasportationVehicles
    {  
       void startMoving() { ... }
    }
~~~
~~~java
    class VehiclesWithEngines extends TrasportationVehicles
    {  
       Engine engine;
       Engine getEngine() { ... }
       void setEngine(Engine e) { ... }
       void startEngine() { ... }
    }
~~~
~~~java
    class Car extends VehiclesWithEngines
    {
       void startEngine() { ... }
    }
~~~
~~~java
    class Bicycle extends VehiclesWithoutEngines
    {
       void startMoving() { ... }
    }
~~~
Now car class extend VehiclesWithEngines and become a more specialize and Bicycle also follow Liskov Substitution Principle.

### Interface segregation principle
Segregation means keeping things separated, and the Interface Segregation Principle is about separating the interfaces. The goal for Interface segregation principle is to reduce and frequency of required changes by splitting into multiple independent parts. This can be achievable by using one general purposeful interface rather than a client-specific interface.

~~~java
public interface ParkingLot {
	void parkCar();
	void removeCar();
	double calculateFee(Car car);
	void doPayment(Car car);
}
class Car {
// body of class
}
~~~
suppose we want to implement a FreeParking class to parkingLot then,
~~~java
public class FreeParking implements ParkingLot {
	public void parkCar() {
		
	}
	public void removeCar() {

	}
	public double calculateFee(Car car) {
		return 0;
	}
	public void doPayment(Car car) {
		throw new Exception("Parking lot is free");
	}
}
~~~
Now here in FreeParking is forced to implement the payment method which is not necessary here.
We can divide ParkingLot into PaidParking and FreeParking classes, it will give much more expandable, flexibility so that our client does not need to implement any irrelevant logic.

### Dependency inversion principle
The Dependency Inversion principle states that our classes should depend upon interfaces or abstract classes instead of concrete classes and functions. The high-level modules which provide complex logic should be reusable and unaffected by the low-level changes.

 - High-level modules should not depend on low-level modules. Both should depend on abstractions.
 - Abstractions should not depend on details. Details should depend on abstractions.


We can better understand this principle by seeing the following example

~~~java

    public class LightBulb {
        public void turnOn() {
            System.out.println("Bulb is ON");
        }
        public void turnOff() {
            System.out.println("Bulb is OFF");
        }
    }
~~~
We have created a LightBulb class with turnOn and turnOff two methods.
Now we create a ElectricSwitch class to access these method,

~~~java
    public class ElectricSwitch {
        public LightBulb lightBulb;
        public boolean on;
        public ElectricSwitch(LightBulb lightBulb) {
            this.lightBulb = lightBulb;
            this.on = false;
        }
        public boolean isOn() {
            return this.on;
        }
        public void press(){
            boolean checkOn = isOn();
            if (checkOn) {
                lightBulb.turnOff();
                this.on = false;
            } else {
                lightBulb.turnOn();
                this.on = true;
            }
        }
    }
~~~

Here we can see that there is a press() method and using this method we are calling turnOn() and turnOff().

But it is violating the Dependency inversion principle, We can see that our high-level class ElectricSwitch is depended upon low-level LightBulb class.

To follow the Dependency Inversion Principle in our example, we will need an abstraction that both the ElectricSwitch and LightBulb classes will depend on. But, before creating it, let’s create an interface for switches.


~~~java
public interface Switch {
    boolean isOn();
    void press();
}
~~~

We wrote an interface for switches with the isOn() and press() methods. This interface will give us the flexibility to plug in other types of switches, say a remote control switch later on if required. Next, we will write the abstraction in the form of an interface, which we will call Switchable.

~~~java
public interface Switchable {
    void turnOn();
    void turnOff();
}
~~~

We have created a  Switchable interface with the turnOn() and turnoff() methods. From now on, any switchable devices in the application can implement this interface and provide their own functionality. Our ElectricSwitch class will also depend on this interface, as shown below:
ElectricPowerSwitch
~~~java
public class ElectricSwitch implements Switch {
    public Switchable client;
    public boolean on;
    public ElectricSwitch(Switchable client) {
        this.client = client;
        this.on = false;
    }
    public boolean isOn() {
        return this.on;
    }
   public void press(){
       boolean checkOn = isOn();
       if (checkOn) {
           client.turnOff();
           this.on = false;
       } else {
             client.turnOn();
             this.on = true;
       }
~~~


In the ElectricSwitch class, we implemented the Switch interface and referred the Switchable interface instead of any concrete class in a field. We then called the turnOn() and turnoff() methods on the interface, which at run time will get invoked on the object passed to the constructor. Now, we can add low-level switchable classes without worrying about modifying the ElectricSwitch class. We will add two such classes: LightBulb and Fan.

~~~java
public class LightBulb implements Switchable {
    public void turnOn() {
        System.out.println("LightBulb: Bulb turned on...");
    }
    public void turnOff() {
        System.out.println("LightBulb: Bulb turned off...");
    }
}
~~~

~~~java
public class Fan implements Switchable {
    public void turnOn() {
        System.out.println("Fan: Fan turned on...");
    }
    public void turnOff() {
        System.out.println("Fan: Fan turned off...");
    }
}
~~~

In both the LightBulb and Fan classes that we wrote, we implemented the Switchable interface to provide their own functionality for turning on and off.

~~~java
public class ElectricSwitchTest {
    public void testPress() throws Exception {
     Switchable switchableBulb=new LightBulb();
     Switch bulbPowerSwitch=new ElectricSwitch(switchableBulb);
     bulbPowerSwitch.press();
     bulbPowerSwitch.press();
    Switchable switchableFan=new Fan();
    Switch fanPowerSwitch=new ElectricSwitch(switchableFan);
    fanPowerSwitch.press();
    fanPowerSwitch.press();
    }
}
~~~


In this way, there is no dependency present in between high-level and low-level classes. And it follows the Dependency inversion principle.

It might be confusing but if we consequently apply open/closed principle and Liskov substitution principle it will also dependency inversion principle.
We need to reorganize our classes to depend on interfaces rather than depending upon concrete classes.

#### Refernces
 - https://en.wikipedia.org/wiki/SOLID
 - https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design
 - https://medium.com/better-programming/solid-principles-simple-and-easy-explanation-f57d86c47a7f# 
